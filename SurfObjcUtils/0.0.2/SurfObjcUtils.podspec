Pod::Spec.new do |s|
  s.name         = "SurfObjcUtils"
  s.version      = "0.0.2"
  s.summary      = "Reusable Objective-C utils developed in Surf"

  s.homepage     = "https://bitbucket.org/surfstudio/surfobjcutils"
  s.license      = "MIT"
  s.author       = { "Dmitry Trofimov" => "dmitry.trofimov@surfstudio.ru" }

  s.platform     = :ios, "8.0"
  s.ios.deployment_target = "8.0"

  s.source       = { :git => "https://bitbucket.org/surfstudio/surfobjcutils", :tag => "#{s.version}" }
  s.source_files = "SurfObjcUtils/**/*.{h,m}"
end
