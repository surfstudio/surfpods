Pod::Spec.new do |s|
  s.name         = "SurfObjcUtils"
  s.version      = "0.1.0"
  s.summary      = "Reusable Objective-C utils developed in Surf"

  s.homepage     = "https://bitbucket.org/surfstudio/surfobjcutils"
  s.license      = "MIT"
  s.author       = { "Dmitry Trofimov" => "dmitry.trofimov@surfstudio.ru" }

  s.platform     = :ios, "8.0"
  s.ios.deployment_target = "8.0"
  s.requires_arc = true

  s.subspec 'Core' do |ss|
    ss.source_files = "SurfObjcUtils/SurfObjcUtils.h", "SurfObjcUtils/Core/**/*.{h,m}"
  end

  s.subspec 'UIKit' do |ss|
    ss.source_files = "SurfObjcUtils/SurfObjcUtils+UIKit.h", "SurfObjcUtils/UIKit/**/*.{h,m}"
    ss.dependency 'SurfObjcUtils/Core'
  end

  s.subspec 'CoreData' do |ss|
    ss.source_files = "SurfObjcUtils/SurfObjcUtils+CoreData.h", "SurfObjcUtils/CoreData/**/*.{h,m}"
    ss.dependency 'SurfObjcUtils/Core'
  end

  s.source       = { :git => "https://bitbucket.org/surfstudio/surfobjcutils", :tag => "#{s.version}" }
  s.default_subspec = 'UIKit'
end
