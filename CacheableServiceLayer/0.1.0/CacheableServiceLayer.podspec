Pod::Spec.new do |s|
  s.name         = "CacheableServiceLayer"
  s.version      = "0.1.0"
  s.summary      = "Reusable logic of service layer, managing cache and server requests"

  s.homepage     = "https://bitbucket.org/surfstudio/cacheableservicelayer"
  s.license      = "MIT"
  s.author       = { "Dmitry Trofimov" => "dmitry.trofimov@surfstudio.ru" }

  s.platform     = :ios, "8.0"
  s.ios.deployment_target = "8.0"

  s.source       = { :git => "https://bitbucket.org/surfstudio/cacheableservicelayer", :tag => "#{s.version}" }
  s.source_files = "CacheableServiceLayer/**/*.{h,m}"
  s.dependency 'SurfObjcUtils', '~> 0.0.3'
  s.dependency 'libextobjc', '~> 0.4'
end
